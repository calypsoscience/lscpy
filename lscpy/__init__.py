"""Define module attributes.
- Defining packaging attributes accessed by setup.py
- Making reading functions available at module level
"""

__version__ = "1.0.0"
__author__ = "Calypso Science"
__contact__ = "r.zyngfogel@calypso.science"
__url__ = "calyps.science"
__description__ = "LSC creattion tools"
__keywords__ = "SCHISM LSC python"