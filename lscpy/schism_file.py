#/usr/bin/env python3
# from the work of  wzhengui
#numpy
import numpy as np
# from numpy import *
# from numpy.random import *
# from numpy.linalg import *
import numpy.ma as ma

import matplotlib as mpl

from pyproj import Proj, transform
import matplotlib.pyplot as plt
#from numpy.fft import *
class schism_grid:
    def __init__(self, fname=None):
        '''
        Initialize to empty instance if fname is not provided;
        otherwise, read from three supported file format
        '''
        if fname is None:
            pass
        elif fname.endswith('gr3') or fname.endswith('.ll'):
            self.read_hgrid(fname)
        else:
            raise Exception('hgrid file format {} not recognized'.format(fname))
        self.source_file = fname

    def transform_to(self,epsgin,epsgout):
        inProj = Proj(init='epsg:'+str(epsgin))
        outProj = Proj(init='epsg:'+str(epsgout))

        self.x,self.y=transform(inProj,outProj,self.x,self.y) 

    def plot_grid(self,ax=None,ax_cb=None,method=0,fmt=0,value=None,mask=None,ec=None,fc=None,
             lw=0.1,levels=None,ticks=None,clim=None,extend='both',cb=True,**args):
        '''
        plot grid with default color value (grid depth)
        method=0: using tricontourf; method=1: using PolyCollection (old method)
        fmt=0: plot grid only; fmt=1: plot color
        value: color value size(np,or ne)
        mask: size(ne); only plot elements (mask=True))
        ec: color of grid line;  fc: element color; lw: grid line width
        levels=100: number of colors for depths; levels=np.array([v1,v2,...]): depths for plot
        ticks=[v1,v2,...]: colorbar ticks; ticks=10: number of ticks
        clim=[vmin,vmax]: value range for plot/colorbar
        cb=False: not add colorbar
        '''

        if ec is None: ec='None'
        if fc is None: fc='None'
        if ax is None: ax=plt.gca()

        if method==0:
           fp3=self.i34==3; fp4=self.i34==4
           # if mask is not None: fp3=fp3*mask; fp4=fp4*mask
           if (fmt==0)|(ec!='None'): #compute lines of grid
              #tri
              tri=self.elnode[fp3,:3]; tri=np.c_[tri,tri[:,0]]
              x3=self.x[tri]; y3=self.y[tri]
              x3=np.c_[x3,np.ones([np.sum(fp3),1])*np.nan]; x3=np.reshape(x3,x3.size)
              y3=np.c_[y3,np.ones([np.sum(fp3),1])*np.nan]; y3=np.reshape(y3,y3.size)
              #quad
              quad=self.elnode[fp4,:]; quad=np.c_[quad,quad[:,0]]
              x4=self.x[quad]; y4=self.y[quad]
              x4=np.c_[x4,np.ones([np.sum(fp4),1])*np.nan]; x4=np.reshape(x4,x4.size)
              y4=np.c_[y4,np.ones([np.sum(fp4),1])*np.nan]; y4=np.reshape(y4,y4.size)

           if fmt==0:
              if ec=='None': ec='k'
              hg=plt.plot(np.r_[x3,x4],np.r_[y3,y4],lw=lw,color=ec);
           elif fmt==1:
              tri=np.r_[self.elnode[(fp3|fp4),:3],np.c_[self.elnode[fp4,0],self.elnode[fp4,2:]]]
              #determine value
              if value is None:
                 value=self.dp
              else:
                 if len(value)==self.ne:
                    value=self.interp_elem_to_node(value=value)
                 elif len(value)!=self.np:
                    sys.exit('value has wrong size: {}'.format(value.shape))

              #detemine clim
              if clim is None:
                 vmin,vmax=min(value),max(value)
              else:
                 vmin,vmax=clim

              #detemine levels
              if levels is None: levels=51
              if np.squeeze(np.array([levels])).size==1:
                 levels=np.linspace(vmin,vmax,int(levels))

              #set mask
              if np.sum(np.isnan(value))!=0: tri=tri[~np.isnan(value[tri].np.sum(axis=1))]

              if vmin==vmax:
                 hg=ax.tricontourf(self.x,self.y,tri,value,vmin=vmin,vmax=vmax,extend=extend,**args)
              else:
                 hg=ax.tricontourf(self.x,self.y,tri,value,levels=levels,vmin=vmin,vmax=vmax,extend=extend,**args)


              #add colobar
              plt.cm.ScalarMappable.set_clim(hg,vmin=vmin,vmax=vmax)
              if cb:
                 #----new method
                 hc=plt.colorbar(hg,ax_cb,ticks=ticks); self.hc=hc
                 if ticks is not None:
                    if not hasattr(ticks,'__len__'):
                       hc.set_ticks(np.linspace(vmin,vmax,int(ticks)))
                    else:
                       hc.set_ticks(ticks)
                 #----old method
                 #hc=colorbar(hg); self.hc=hc;
                 #if ticks is not None: hc.set_ticks(ticks)
                 #hc.set_clim([vmin,vmax]);

              #plot grid
              if ec!='None': hg=plt.plot(np.r_[x3,x4],np.r_[y3,y4],lw=lw,color=ec);

           self.hg=hg; #show(block=False
           if mpl.get_backend().lower() in ['qt5agg']:

              acs=plt.gcf().canvas.toolbar.actions(); ats=np.array([i.iconText() for i in acs])
              # if 'bp' not in ats: self.bp=schism_bpfile()
              # if 'query' not in ats: self.query_pt()
              # if 'bnd' not in ats: self.create_bnd()
           return hg,hc



    def read_hgrid(self,fname,*args):
        #attribute tracking the file originally read, mainly used for savez and save_pkl
        self.source_file = fname  

        fid=open(fname,'r'); lines=fid.readlines(); fid.close()

        #read ne and np; lx,ly and dp
        self.ne,self.np=np.array(lines[1].split()[0:2]).astype('int')
        self.x,self.y,self.dp=np.array([i.split()[1:4] for i in lines[2:(2+self.np)]]).astype('float').T
        if len(lines)<(2+self.np+self.ne): return

        #read elnode and i34
        fdata=[i.strip().split() for i in lines[(2+self.np):(2+self.np+self.ne)]]
        fdata=np.array([i if len(i)==6 else [*i,'-1'] for i in fdata]).astype('int')
        self.i34=fdata[:,1]; self.elnode=fdata[:,2:]-1; fdata=None
        
        #compute ns
        self.compute_side()
        if len(lines)<(4+self.np+self.ne): return

        #read open bnd info
        n=2+self.np+self.ne; self.nob=int(lines[n].strip().split()[0]); n=n+2; self.nobn=[]; self.iobn=[]
        for i in np.arange(self.nob):
            self.nobn.append(int(lines[n].strip().split()[0]))
            self.iobn.append(np.array([int(lines[n+1+k].strip().split()[0])-1 for k in np.arange(self.nobn[-1])]))
            n=n+1+self.nobn[-1]
        self.nobn=np.array(self.nobn); self.iobn=np.array(self.iobn,dtype='O')
        if len(self.iobn)==1: self.iobn=self.iobn.astype('int')

        #read land bnd info
        self.nlb=int(lines[n].strip().split()[0]); n=n+2; self.nlbn=[]; self.ilbn=[]; self.island=[]
        for i in np.arange(self.nlb):
            sline=lines[n].split('=')[0].split(); self.nlbn.append(int(sline[0])); ibtype=0
            self.ilbn.append(np.array([int(lines[n+1+k].strip().split()[0])-1 for k in np.arange(self.nlbn[-1])]))
            n=n+1+self.nlbn[-1]

            #add bnd type info
            if len(sline)==2: ibtype=int(sline[1])
            if self.ilbn[-1][0]==self.ilbn[-1][-1]: ibtype=1
            self.island.append(ibtype)
        self.island=np.array(self.island); self.nlbn=np.array(self.nlbn); self.ilbn=np.array(self.ilbn,dtype='O');
        if len(self.ilbn)==1: self.ilbn=self.ilbn.astype('int')

    def interp_node_to_elem(self,value=None):
        '''
        interpolate node values to element values
            default is self.dp => self.dpe
        '''
        #get node value
        if value is None:
            dp=self.dp
        else:
            dp=value

        #interpolate
        fp1=self.i34==3; fp2=self.i34==4;
        dpe=np.zeros(self.ne)*np.nan
        dpe[fp1]=np.mean(dp[self.elnode[fp1,0:3]],axis=1)
        dpe[fp2]=np.mean(dp[self.elnode[fp2,:]],axis=1)

        return dpe

    def interp_elem_to_node(self,value=None,fmt=0,p=1):
        '''
        interpolate element values to nodes
        if value not given, dpe is used
        fmt=0: simple avarage; fmt=1: inverse distance (power=p)
        fmt=2: maximum of surrounding nodal values
        '''
        #-specify element values
        if value is None:
            if not hasattr(self,'dpe'): self.compute_ctr()
            v0=self.dpe
        else:
            v0=value;

        #compute node ball
        if not hasattr(self,'nne'): self.compute_node_ball()

        #interpolation
        v=[];
        for i in np.arange(self.np):
            ind=self.ine[i];
            if fmt==0: #aveaging
                vi=np.sum(v0[ind])/self.nne[i];
            elif fmt==1: #inverse distance
                W=1/((self.xctr[ind]-self.x[i])**2+(self.yctr[ind]-self.y[i])**2)**(p/2); #weight
                vi=np.sum(W*v0[ind])/np.sum(W)
            elif fmt==2: #maximum of surrounding nodal values
                vi=max(v0[ind]);
            else:
                raise Exception('fmt: {} undefined\n'.format(fmt))
            v.append(vi)
        v=np.array(v)
        return v

    def compute_ctr(self):
        '''
        compute element center XYZ
        '''
        if not hasattr(self,'xctr'):
           fp1=self.i34==3; fp2=self.i34==4;
           self.xctr=np.zeros(self.ne)*np.nan
           self.yctr=np.zeros(self.ne)*np.nan

           self.xctr[fp1]=np.mean(self.x[self.elnode[fp1,0:3]],axis=1)
           self.yctr[fp1]=np.mean(self.y[self.elnode[fp1,0:3]],axis=1)
           self.xctr[fp2]=np.mean(self.x[self.elnode[fp2,:]],axis=1)
           self.yctr[fp2]=np.mean(self.y[self.elnode[fp2,:]],axis=1)

        self.dpe=self.interp_node_to_elem()
        return self.dpe

    def compute_area(self):
        fp=self.elnode[:,-1]<0;
        x1=self.x[self.elnode[:,0]]; y1=self.y[self.elnode[:,0]];
        x2=self.x[self.elnode[:,1]]; y2=self.y[self.elnode[:,1]];
        x3=self.x[self.elnode[:,2]]; y3=self.y[self.elnode[:,2]];
        x4=self.x[self.elnode[:,3]]; y4=self.y[self.elnode[:,3]]; x4[fp]=x1[fp]; y4[fp]=y1[fp]
        self.area=((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1)+(x3-x1)*(y4-y1)-(x4-x1)*(y3-y1))/2
        return self.area

    def compute_side(self,fmt=0):
        '''
        compute side information of schism's hgrid 
        fmt=0: compute ns (# of sides) only; fmt=1: compute ns,isidenode and isdel
        '''

        #collect sides
        fp3=np.nonzero(self.i34==3)[0]; fp4=np.nonzero(self.i34==4)[0]; sis=np.array([[],[]]).astype('int').T; sie=np.array([]).astype('int')
        for i in np.arange(3): sis=np.r_[sis,np.c_[self.elnode[fp3,np.mod(i+3,3)],self.elnode[fp3,np.mod(i+4,3)]]]; sie=np.r_[sie,fp3]
        for i in np.arange(4): sis=np.r_[sis,np.c_[self.elnode[fp4,np.mod(i+4,4)],self.elnode[fp4,np.mod(i+5,4)]]]; sie=np.r_[sie,fp4]
        
        #np.sort side
        sis=np.sort(sis,axis=1).T; usis,sind=np.unique(sis[0]+1j*sis[1],return_inverse=True); self.ns=len(usis)

        #compute isdel and isidenode
        if fmt==1 and not hasattr(self,'isdel'): 
           ps=[[] for i in np.arange(self.ns)];  t=[ps[i].append(k) for i,k in zip(np.arange(self.ns)[sind], sie)]
           self.isdel=np.array([i if len(i)==2 else [*i,-1] for i in ps])
           self.isidenode=np.c_[np.real(usis),np.imag(usis)].astype('int')

        return self.ns

    def compute_bnd(self):
        '''
        compute boundary information
        '''
        print('computing grid boundaries')
        if not hasattr(self,'isdel') or not hasattr(self,'isidenode'): self.compute_side(fmt=1)

        #find boundary side and element
        fpn=fp=self.isdel[:,-1]==-1;  isn=self.isidenode[fpn]; be=self.isdel[fpn][:,0]; nbs=len(be)
        
        #np.sort isn
        i2=ones(nbs).astype('int'); fp3=np.nonzero(self.i34[be]==3)[0]; fp4=np.nonzero(self.i34[be]==4)[0]
        for i in np.arange(4):
            if i==3:
                i1=self.elnode[be[fp4],3]; i2=self.elnode[be[fp4],0]
                fp=(isn[fp4,0]==i2)*(isn[fp4,1]==i1); isn[fp4[fp]]=fliplr(isn[fp4[fp]])
            else:
                i1=self.elnode[be,i]; i2[fp3]=self.elnode[be[fp3],(i+1)%3]; i2[fp4]=self.elnode[be[fp4],i+1]
                fp=(isn[:,0]==i2)*(isn[:,1]==i1); isn[fp]=fliplr(isn[fp])        

        #compute all boundaries
        sinds=dict(zip(isn[:,0],np.arange(nbs))) #dict for sides
        ifb=ones(nbs).astype('int'); nb=0; nbn=[]; ibn=[]
        while(np.sum(ifb)!=0):
            #start points
            id0=isn[np.nonzero(ifb==1)[0][0],0]; id=isn[sinds[id0],1]; ibni=[id0,id]
            ifb[sinds[id0]]=0; ifb[sinds[id]]=0;
            while True:
                id=isn[sinds[id],1]; ifb[sinds[id]]=0
                if(id==id0): break
                ibni.append(id)
            nb=nb+1; nbn.append(len(ibni)); ibn.append(np.array(ibni))

        #save boundary information
        if not hasattr(self,'bndinfo'): self.bndinfo=zdata() 
        ip=[]; sind=[]; S=self.bndinfo 
        for m,ibni in enumerate(ibn): ip.extend(ibni); sind.extend(np.tile(m,len(ibni)))
        ip=np.array(ip); sind=np.array(sind); S.sind=sind; S.ip=ip; S.island=ones(nb).astype('int')
        S.nb=nb; S.nbn=np.array(nbn); S.ibn=np.array(ibn); S.x=self.x[ip]; S.y=self.y[ip]

        #find the outline
        for i in np.arange(nb):
            px=self.x[S.ibn[i]]; i0=np.nonzero(px==px.min())[0][0]
            sid=S.ibn[i][np.array([(i0-1)%S.nbn[i],i0,(i0+1)%S.nbn[i]])]
            if signa(self.x[sid],self.y[sid])>0: S.island[i]=0; break

        #add to grid bnd info
        if not hasattr(self,'nob'): 
           self.nob=0; self.nobn=np.array([]); self.iobn=np.array([[]]); sind=argnp.sort(S.island)
           self.nlb=S.nb; self.nlbn=S.nbn[sind]; self.ilbn=S.ibn[sind]; self.island=S.island[sind]

    def compute_node_ball(self):
        '''
        compute nodal ball information
        '''
        nne=np.zeros(self.np).astype('int');
        ine=[[] for i in np.arange(self.np)];
        for i in np.arange(self.ne):
            inds=self.elnode[i,:self.i34[i]];
            nne[inds]=nne[inds]+1
            [ine[indi].append(i) for indi in inds]
        self.nne=nne
        self.ine=np.array([np.array(ine[i]) for i in np.arange(self.np)],dtype='O');
        return self.nne, self.ine

    def compute_acor(self,pxy):
        '''
        compute acor coodinate for points pxy[npt,2]

        usage: ie,ip,acor=compute_acor(np.c_[xi,yi]), where xi and yi are np.array of coordinates
        outputs: ie[npt],ip[npt,3],acor[npt,3]
               ie:  the element number
               ip:  the nodal indices of the ie
               acor: the area coordinate
        '''
        #compute parents element
        pie,pip=self.inside_grid(pxy,fmt=1); fpn=pie!=-1

        #cooridate for triangles, and areas
        x1,x2,x3=self.x[pip[fpn]].T; y1,y2,y3=self.y[pip[fpn]].T; x,y=pxy[fpn].T
        A=signa(np.c_[x1,x2,x3],np.c_[y1,y2,y3]); A1=signa(np.c_[x,x2,x3],np.c_[y,y2,y3])
        A2=signa(np.c_[x1,x,x3],np.c_[y1,y,y3]);  #A3=signa(np.c_[x1,x2,x],np.c_[y1,y2,y])

        #area coordinate
        pacor=np.zeros(pip.shape); pacor[fpn]=np.c_[A1/A,A2/A,1-(A1+A2)/A]
        return [pie,pip,pacor]

    def interp(self,xyi):
        #interpolate to get depth at xyi
        ie,ip,acor=self.compute_acor(xyi)
        dpi=(self.dp[ip]*acor).np.sum(axis=1)
        return dpi


    def write_hgrid(self,fname,value=None,fmt=0,elnode=1,bndfile=None,Info=None):
        '''
        write *.gr3 file
            fname: file name
            value: depth value to be outputted
                   value=const: uniform value in space
                   value=dp[np]: specify depth value
                   value=None:  grid's default depth self.dp is used
            fmt=0: not output grid boundary info.; fmt=1: output grid boundary info. 
            elnode=1: output grid connectivity; elnode=0: not output grid connectivity
            bndfile=filepath:  if bndfile is not None, append it at the end of file
            Info: annotation of the gr3 file
        '''

        #get depth value
        if value is None:
           dp=self.dp
        else:
           if hasattr(value,"__len__"):
              dp=value
           else:
              dp=ones(self.np)*value
        if fmt==1: elnode=1

        #write *gr3
        with open(fname,'w+') as fid:
            fid.write('!grd info:{}\n'.format(Info))
            fid.write('{} {}\n'.format(self.ne,self.np))
            for i in np.arange(self.np):
                fid.write('{:<d} {:<.8f} {:<.8f} {:<.8f}\n'.format(i+1,self.x[i],self.y[i],dp[i]))
            if elnode!=0:
                for i in np.arange(self.ne):
                    if self.i34[i]==3: fid.write('{:<d} {:d} {:d} {:d} {:d}\n'.format(i+1,self.i34[i],*self.elnode[i,:]+1))
                    if self.i34[i]==4: fid.write('{:<d} {:d} {:d} {:d} {:d} {:d}\n'.format(i+1,self.i34[i],*self.elnode[i,:]+1))

            #write bnd information
            if fmt==1 and bndfile is None: self.write_bnd(fid=fid) 
            if bndfile is not None: fid.writelines(open(bndfile,'r').readlines())
    
    def write_bnd(self,fname='grd.bnd',fid=None):
        '''
        write grid's boundary information
            fname: name of boundary information 
            fid: file handle
        '''
        if not hasattr(self,'nob'): self.compute_bnd()
        bid=open(fname,'w+') if fid is None else fid

        #open bnd
        bid.write('{} = Number of open boundaries\n'.format(self.nob))
        bid.write('{} = Total number of open boundary nodes\n'.format(int(np.sum(self.nobn))))
        for i in np.arange(self.nob):
            bid.write('{} = Number of nodes for open boundary {}\n'.format(self.nobn[i],i+1)) 
            bid.writelines(['{}\n'.format(k+1) for k in self.iobn[i]])

        #land bnd
        bid.write('{} = number of land boundaries\n'.format(self.nlb))
        bid.write('{} = Total number of land boundary nodes\n'.format(int(np.sum(self.nlbn)))); nln=int(np.sum(self.island==0))
        for i in np.arange(self.nlb):
            if self.island[i]==0:
               bid.write('{} {} = Number of nodes for land boundary {}\n'.format(self.nlbn[i],self.island[i],i+1)) 
            else:
               bid.write('{} {} = Number of nodes for island boundary {}\n'.format(self.nlbn[i],self.island[i],i+1-nln)) 
            bid.writelines(['{}\n'.format(k+1) for k in self.ilbn[i]])
        if fid is not None: bid.close()



    def split_quads(self,angle_min=60,angle_max=120,fname='new.gr3'):
        '''
        1). split the quads that have angle (<angle_min or >angle_max), add append the connectivity in the end
        2). output a new grid "fname"
        '''
        if not hasattr(self,'index_bad_quad'): self.check_quads(angle_min,angle_max)

        #compute (angle_max-angle_min) in splitted triangle
        qind=self.index_bad_quad;
        x=self.x[self.elnode[qind,:]]; y=self.y[self.elnode[qind,:]];

        #compute difference between internal angles
        for i in np.arange(4):
            id1=np.mod(i-1+4,4); id2=i; id3=np.mod(i+1,4)
            x1=x[:,id1]; x2=x[:,id2]; x3=x[:,id3];
            y1=y[:,id1]; y2=y[:,id2]; y3=y[:,id3];

            a1=angle((x1-x2)+1j*(y1-y2))-angle((x3-x2)+1j*(y3-y2))
            a2=angle((x2-x3)+1j*(y2-y3))-angle((x1-x3)+1j*(y1-y3))
            a3=angle((x3-x1)+1j*(y3-y1))-angle((x2-x1)+1j*(y2-y1))
            a1=np.mod(a1*180/pi+360,360);a2=np.mod(a2*180/pi+360,360);a3=np.mod(a3*180/pi+360,360);

            #compute amax-amin
            a=np.c_[a1,a2,a3];
            Ai=a.max(axis=1)-a.min(axis=1)
            if i==0:
                A=Ai
            else:
                A=np.c_[A,Ai]

        #split quads
        flag=sign(A[:,0]+A[:,2]-A[:,1]-A[:,3])

        ne=self.ne; nea=len(self.index_bad_quad);
        self.elnode=np.r_[self.elnode,ones([nea,4])-3].astype('int');
        for i in np.arange(nea):
            ind=self.index_bad_quad[i]
            nds=self.elnode[ind,:].copy();
            if flag[i]>=0:
                self.elnode[ind,:]=np.r_[nds[[0,1,2]],-2]; self.i34[ind]=3
                self.elnode[ne+i,:]=np.r_[nds[[2,3,0]],-2]
            else:
                self.elnode[ind,:]=np.r_[nds[[1,2,3]],-2]; self.i34[ind]=3
                self.elnode[ne+i,:]=np.r_[nds[[3,0,1]],-2]

        self.ne=ne+nea
        self.i34=np.r_[self.i34,ones(nea)*3].astype('int');
        self.elnode=self.elnode.astype('int')

        #write new grids
        self.write_hgrid(fname)



    def proj(self,prj0,prj1='epsg:4326',x=None,y=None,lon0=None,lat0=None):
        '''
        transform the projection of schism grid's coordinates
        Inputs:
            prj0: projection name of schism grid
            prj1: target projection name; default is 'epsg:4326'
            x,y: values of grid coordiantes; default is (gd.x, gd.y)
            lon0,lat0: lon&lat of cpp projection center; needed only if 'cpp' in [prj0,prj1]
                       if ("ll"=>"cpp") and (lon0 or lat0 is not provided): lon0=np.mean(x); lat0=np.mean(y)
        '''
        if (x is None) or (y is None): x=self.x; y=self.y
        x1,y2=proj(prj0=prj0,prj1=prj1,x=x,y=y,lon0=lon0,lat0=lat0)
        return [x1,y2]

   
    def inside_grid(self,pxy,fmt=0):
        '''
          compute element indices that pxy[npt,2] resides. '-1' np.means outside of the grid domain
          usage:
               sind=inside_grid(pxy)
               sind,ptr=inside_grid(pxy,fmt=1)
               ptr is triange indice (=1:1st triangle; =2: 2nd triangle), used for computing area coordinates
        '''
        #first triangles
        sindp=self.elnode[:,:3]; px=self.x[sindp]; py=self.y[sindp]
        pie=inside_polygon(pxy,px.T,py.T,fmt=1); fp1=pie!=-1; sind2=np.nonzero(~fp1)[0]
        if fmt==1: pip=-ones([len(pxy),3]).astype('int'); pip[fp1]=sindp[pie[fp1]]

        #2nd triangles
        if len(sind2)!=0:
           fp34=self.i34==4; sindp=self.elnode[fp34,:][:,np.array([0,2,3])]; px=self.x[sindp]; py=self.y[sindp]
           pie2=inside_polygon(pxy[sind2],px.T,py.T,fmt=1); fp2=pie2!=-1; pie[sind2[fp2]]=np.nonzero(fp34)[0][pie2[fp2]]
           if fmt==1: pip[sind2[fp2]]=sindp[pie2[fp2]]

        if fmt==0:
           return pie
        else:
           return [pie,pip]




def read_schism_hgrid(fname):
    gd=schism_grid()
    gd.read_hgrid(fname)
    return gd

def save_schism_grid(fname='grid',path='.'):
    '''
    read and save path/{hgrid.gr3,vgrid.in}
    '''
    gname='{}/hgrid.gr3'.format(path); vname='{}/vgrid.in'.format(path); S=zdata();
    if os.path.exists(gname): S.hgrid=read_schism_hgrid(gname)
    if os.path.exists(vname): S.vgrid=read_schism_vgrid(vname)
    if (not hasattr(S,'hgrid')) and (not hasattr(S,'vgrid')): sys.exit('not found: {}, {}'.format(gname,vname))
    savez(fname,S)
    return S


if __name__=="__main__":
    pass

