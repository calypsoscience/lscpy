Lscpy
-------

Python GUI to help create a SCHISM LSC vertical grid.


.. image:: screenshot.png


Documentation:
--------------
Not yet available

Main contents:
--------------
- lsp.py       : gui launcher
- vqs.py       : functions and opions

Install:
--------

Install requirements. Navigate to the base root of toto and execute:

.. code:: bash

   pip install -r requirements.txt


Launching:
---------

.. code:: bash

   python name_of_the_grid.gr3 EPSGin EPSGout

-  ``EPSGin`` (int): original EPSG value of the input grid. (i.e 4326)
-  ``EPSGout`` (int): cartesian EPSG value of the input grid. (i.e 2193) 

The grid need to be in Cartesian coordiante, so ``EPSGin`` and ``EPSGout`` are needed if the inital grid is in geographic coordinate


Examples - Loading a grid:
--------------------------

If reading a grid in WGS84 and transforming it in NZTM

.. code:: bash
   
   python lsc.py hgrid.gr3 4326 2193

If reading a grid in NZTM coordinates

.. code:: bash
   
   python lsc.py hgrid.gr3

Examples - Create master grids:
-------------------------------

1- 
   First enter the depth of each master grid in the box next to ``Depth of each master grid``. Important the last depth must greater or equal to the maximum depth in the grid

2- 
   Enter the number of levels for each master grid in the box next to ``Number of level at each master grid``. Here a custom expression can be enter:

.. code:: python

   10+1*(N)

   In the GUI N will be replaced by:

.. code:: python

   np.range(0, len(master grid)) 


3- 
   Press enter

4- 
   Click on ``transect`` at the bottom of the grid view plot. Create a transect somewhere in the grid
   by clicking in the grid. Double click to end the transect.

5- 
   The sigma layer underneath the transect that has been created is plotted on the right hand side.

6- 
   Click on ``view`` underneath the grid view plot to switch between number of levels and depth.

7- 
   To edit manually the Vgrid that has been created, select ``selector`` at the bottom of the grid view plot. then press and hold the left button on the mouse and select an area on the map. Release the button. Keep selecting areas as needed. When finished click ``selector`` button. Enter the number of level you want for those area. BECARFULL, if you update any other options your changes will be LOST.

8- 
   Export you LSC grid by going to ``vgrid`` menu at the top of the GUI. You can also export the parameters you have used as yaml

Examples - Custom expressions:
------------------------------

When using S-layer, it is sometimes necessary to vary the theta-b or theta-f
for each master grid. To do so enter an expression in the box next to theta-f or theta-b
such as:

.. code:: python

   0.1+1*(N)

In the GUI N will be replaced by:

.. code:: python

   np.range(0, len(master grid)) 

 

